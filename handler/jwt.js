const fs   = require('fs');
const path = require('path')
const jwt   = require('jsonwebtoken');
const Constants = require("../constants/Constants");

// use 'utf8' to get string instead of byte array  (512 bit key)
var privateKEY  = fs.readFileSync(path.join(__dirname,'private.key'), 'utf8');
var publicKEY  = fs.readFileSync(path.join(__dirname,'public.key'), 'utf8');  
module.exports = {
 sign: (payload) => {

  return jwt.sign(payload, privateKEY, Constants.JWTOptions);
},
verify: (token) => {
   try{
     return jwt.verify(token, publicKEY, Constants.JWTOptions);
   }catch (err){
     return false;
   }
},
 decode: (token) => {
    return jwt.decode(token, {complete: true});
    //returns null if token is invalid
 }
}