require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');
const { toDefaultValue } = require('sequelize/types/lib/utils');
const sequelize = new Sequelize(process.env.DB_URI);
const CAS_M_MOE_STUDENT = sequelize.define('CAS_M_MOE_STUDENT', {
  // Model attributes are defined here
  CIVIL_NUM: {
    type: DataTypes.STRING(20),
    unique:true
  },
  STUDENT_ID:{
    type:DataTypes.STRING(20),
    allowNull:false,
    primaryKey:true
  },
  FIRST_NAME: {
    type: DataTypes.STRING(50)
  },
  FIRST_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  SECOND_NAME: {
    type: DataTypes.STRING(50)
  },
  SECOND_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  THIRD_NAME: {
    type: DataTypes.STRING(50)
  },
  THIRD_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  FOURTH_NAME: {
    type: DataTypes.STRING(50)
  },
  FOURTH_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  DATE_OF_BIRTH:{
    type:DataTypes.DATE,
    // get() {
    //   return moment(this.getDataValue('dob')).format('DD/MM/YYYY');
    // }
  },
  PLACE_OF_BIRTH:{
    type:DataTypes.STRING(50)
},
  PLACE_OF_BIRTH_AR:{
      type:DataTypes.STRING(100)
  },
  STATUS:{
    type:DataTypes.NUMBER(2,0)
 },
 AUDIT_TRAIL_ID:{
    type:DataTypes.NUMBER(38,0)
 },
 GENDER:{
    type:DataTypes.CHAR(1)
  },
  SCHOOL_ID:{
    type:DataTypes.STRING(10)
},
SCHOOL_SEAT_NUMBER:{
    type:DataTypes.STRING(10)
},
SCHOOL_STUDY_YEAR:{
    type:DataTypes.STRING(10)
},
PHONE:{
    type:DataTypes.STRING(20)
 },
 EMAIL:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS2:{
    type:DataTypes.STRING(100)
 },
 ADDRESS2_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS3:{
    type:DataTypes.STRING(100)
 },
 ADDRESS3_AR:{
    type:DataTypes.STRING(200)
 },

 GSM:{
  type:DataTypes.STRING(20),
  allowNull:false
},

STUDENT_TYPE:{
  type:DataTypes.NUMBER(2,0)
},

 GUARDIAN_NAME:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_NAME_AR:{
    type:DataTypes.STRING(200)
 },
 RELATION_ID:{
    type:DataTypes.STRING(5)
 },
 PLACE_OF_WORK:{
    type:DataTypes.STRING(50)
 },
 PLACE_OF_WORK_AR:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_ADDRESS1:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS1_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS2:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS2_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS3:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS3_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_PO_BOX_NUM:{
    type:DataTypes.STRING(10)
  },
  GUARDIAN_POSTAL_CODE:{
    type:DataTypes.STRING(10)
  },
  GUARDIAN_WILAYAT_ID:{
    type:DataTypes.STRING(20)
  },
  WILAYAT_ID:{
    type:DataTypes.STRING(20)
  },
  GUARDIAN_GSM:{
    type:DataTypes.STRING(20)
  },
  PASSPORT_NUM:{
    type:DataTypes.STRING(20),
    unique:true
  },
  RELIGION_ID:{
    type:DataTypes.STRING(5)
  },

  COUNTRY_ID:{
    type:DataTypes.STRING(5),
    defaultValue:512
  },

  VILLAGE_ID:{
    type:DataTypes.STRING(20)
  },
  CERTIFICATE_ID:{
    type:DataTypes.STRING(20),
    allowNull:false
  },

  CAO_NUM:{
    type:DataTypes.STRING(20)
  },
  REG_STATUS:{
    type:DataTypes.NUMBER(2,0)
  },
  UP_STATUS:{
    type:DataTypes.STRING(2)
  },
  MARK_STATUS:{
    type:DataTypes.STRING(2)
  },
  NATIONALITY_PARTIAL:{
    type:DataTypes.NUMBER(2,0),
    defaultValue:0
  },

  GCE_STUDENT_CONFIRM:{
    type:DataTypes.NUMBER(2,0)
  },

  IS_DISABLE:{
    type:DataTypes.NUMBER(3,0)
  },
  
  DISABLE_ID:{
    type:DataTypes.STRING(20)
  },
  
  EXAM_COUNTRY_ID:{
    type:DataTypes.STRING(5)
 },
 QUALIFICATION_ID:{
    type:DataTypes.STRING(10)
 },

 VERIFIED_DISABILITY:{
  type:DataTypes.NUMBER(1,0)
},

DISABILITY_VERIFIED_DATE:{
  type:DataTypes.DATE
},

DISABILITY_REMARK:{
  type:DataTypes.STRING(200)
},

IS_PG:{
  type:DataTypes.NUMBER(1,0)
},
ORG_JOIN_DATE:{
  type:DataTypes.DATE
},

CURRENT_PROFESSIOM:{
  type:DataTypes.STRING(200)
},

PG_CERTIFICATE_NAME:{
  type:DataTypes.STRING(40)
},


 REGISTRATION_MODE:{
  type:DataTypes.STRING(15)
},

REGISTERED_LINK:{
  type:DataTypes.NUMBER(1,0),
  defaultValue:1
},
IS_CONCESSION:{
  type:DataTypes.NUMBER(1,0),
  defaultValue:0
},

VERIFIED_CONCESSION:{
  type:DataTypes.NUMBER(1,0)
},

VERIFIED_CONCESSION_DATE:{
  type:DataTypes.DATE
},

CONCESSION_USER_ID:{
  type:DataTypes.STRING(20),
  defaultValue:0
},

 REGISTER_DATE:{
    type:DataTypes.DATE,
    defaultValue:Sequelize.NOW
 },
 REMARKS:{
    type:DataTypes.STRING(500)
 },
 NATIONALITY_STATUS:{
    type:DataTypes.STRING(200)
 },
 USERCHANG:{
    type:DataTypes.STRING(200)
 },

 MEDICAL_STATUS:{
  type:DataTypes.NUMBER(1,0),
  defaultValue:2
},

MEDICAL_VERIFIED_DATE:{
  type:DataTypes.DATE,
  defaultValue:0
},

MEDICAL_VERIFIED_USERID:{
  type:DataTypes.STRING(20),
},

UNCONVERT_DATE:{
  type:DataTypes.DATE,
},

UNCONVERT_USER_ID:{
  type:DataTypes.STRING(20),
},

MARK_ELIGIBILITY_OVERRIDE:{
  type:DataTypes.STRING(2),
  defaultValue:0
},

AUDIT_VERIFY_STATUS:{
  type:DataTypes.STRING(1)
},

AVG_MARK:{
  type:DataTypes.NUMBER(8,4)
},

ID_CARD_EXPIRY_DATE:{
  type:DataTypes.DATE
},

BIRT_PLACE_COUNTRY_ID:{
  type:DataTypes.STRING(20)
},

BIRT_WILAYAT_ID:{
  type:DataTypes.STRING(20)
},

IS_MEDICAL_ISSUE:{
  type:DataTypes.NUMBER(0,1)
},

 CONVERT_DATE:{
    type:DataTypes.DATE
 },
 ELIGIBLE_STATUS:{
    type:DataTypes.STRING(1),
    defaultValue:0
 },


 PREV_CITIZENSHIP_COUNTRY_ID:{
  type:DataTypes.STRING(20)
},

GUARDIAN_RELATIONSHIP_ID:{
  type:DataTypes.STRING(20)
},

GUARDIAN_WORK_PHONE:{
  type:DataTypes.STRING(20)
},

PASSPORT_ISSUE_DATE:{
  type:DataTypes.DATE
},

PASSPORT_EXPIRY_DATE:{
  type:DataTypes.DATE
},
IS_DETAILS_CHANGE:{
  type:DataTypes.STRING(10),
  defaultValue:0
},
AGREE_TERMS:{
  type:DataTypes.NUMBER(4,0),
  defaultValue:0
},

EQUIVALENCE_CERTIFICATE:{
  type:DataTypes.STRING(1)
},

IS_EXCLUDE_SE:{
  type:DataTypes.NUMBER,
  defaultValue:0
},

BANK_BRANCH_NAME:{
  type:DataTypes.STRING(400)
},

STUDY_TYPE:{
  type:DataTypes.STRING(1),
  defaultValue:0
},

STUDY_TYPE:{
  type:DataTypes.STRING(1),
  defaultValue:0
},

SCHOOL_TYPE:{
  type:DataTypes.NUMBER,
  defaultValue:0
},

KSA_STATUS:{
  type:DataTypes.STRING(0),
  defaultValue:0
},

MOB_STATUS:{
  type:DataTypes.NUMBER,
  defaultValue:0
},

REASON_COMMENTS:{
  type:DataTypes.STRING(400)
},
VERIFIED_DE:{
  type:DataTypes.NUMBER,
  defaultValue:2
},

DE_VERIFIED_DATE:{
  type:DataTypes.DATE
},

DE_REMARK:{
  type:DataTypes.STRING(4000)
},

VERIFIED_DE_USERID:{
  type:DataTypes.STRING(20)
},

VERIFIED_IELTS:{
  type:DataTypes.NUMBER,
  defaultValue:2
},

VERIFIED_IELTS_DATE:{
  type:DataTypes.DATE
},

VERIFIED_IELTS_USERID:{
  type:DataTypes.STRING(20)
},

ISLOW_INCOME_PER_MONTH:{
  type:DataTypes.NUMBER(1,0)
},
DISABLE_TYPE_ID:{
  type:DataTypes.STRING(20)
},
 TUWTINEMTIAZ:{
     type:DataTypes.STRING(3),
     defaultValue:0
 },
 PASSWORD:{
  type:DataTypes.STRING(20),
  allowNull:false
 },
 IS_GCC:{
     type:DataTypes.NUMBER(0,1),
     defaultValue:0
 },   
},
{
  indexes: [
    {
        unique: true,
        fields: ['SCHOOL_STUDY_YEAR', 'SCHOOL_SEAT_NUMBER']
    }
],
  timestamps: false,
  freezeTableName:true,
  // Other model options go here
  hooks: {
    beforeCreate: async (user) => {
     if (user.password) {
      const salt = await bcrypt.genSaltSync(10, 'a');
      user.password = bcrypt.hashSync(user.password, salt);
     }
    },
    beforeUpdate:async (user) => {
     if (user.password) {
      const salt = await bcrypt.genSaltSync(10, 'a');
      user.password = bcrypt.hashSync(user.password, salt);
     }
    }
   },
   instanceMethods: {
    validPassword: (password) => {
     return bcrypt.compareSync(password, this.password);
    }
   }
}
);

module.exports = CAS_M_MOE_STUDENT;