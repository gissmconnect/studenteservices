require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');
const sequelize = new Sequelize(process.env.DB_URI);
const Student = sequelize.define('Student', {
  // Model attributes are defined here
  phoneNumber: {
    type: DataTypes.STRING(10),
    allowNull: false
  },
  civilNum:{
    type:DataTypes.STRING,
    unique:true,
    primaryKey:true
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: false
  },
  secondName: {
    type: DataTypes.STRING
    // allowNull defaults to true
  },
  thirdName:{
    type: DataTypes.STRING
  },
  fourthName:{
    type: DataTypes.STRING
  },
  gender:{
    type:DataTypes.ENUM,
    values:['Male','Female']
  },
  dob:{
    type:DataTypes.DATE,
    get() {
      return moment(this.getDataValue('dob')).format('DD/MM/YYYY');
    }
  },
  password:{
    type:DataTypes.STRING,
    allowNull:false
  },
  age:{
    type:DataTypes.INTEGER,
    get(){
      return moment().diff(this.getDataValue('dob'), 'years');
    }
  },
  email:{
    type: DataTypes.STRING
  },
  gceExam:{
    type: DataTypes.STRING
  }
},
 {
  // Other model options go here
    hooks: {
      beforeCreate: async (user) => {
       if (user.password) {
        const salt = await bcrypt.genSaltSync(10, 'a');
        user.password = bcrypt.hashSync(user.password, salt);
       }
      },
      beforeUpdate:async (user) => {
       if (user.password) {
        const salt = await bcrypt.genSaltSync(10, 'a');
        user.password = bcrypt.hashSync(user.password, salt);
       }
      }
     },
     instanceMethods: {
      validPassword: (password) => {
       return bcrypt.compareSync(password, this.password);
      }
     }
});
Student.sync();
module.exports = Student;