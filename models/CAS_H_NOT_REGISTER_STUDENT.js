require('dotenv').config()
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const NStudent = sequelize.define('CAS_H_NOTREGISTER_STUDENTS', {
  // Model attributes are defined here
  CIVIL_NUM: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  USER_ID: {
    type: DataTypes.STRING(20),
    allowNull: false
  },
  REG_STATUS: {
    type: DataTypes.STRING(1),
    comment:"0 1 ALLOWED ONLY"
  },
  CREATION_DATE:{
    type: DataTypes.DATE,
    default:Sequelize.NOW
  },
  STUDENT_ID:{
      TYPE:DataTypes.STRING(20)
  }
},{
    timestamps: false,
    freezeTableName:true
}
);
NStudent.sync();
module.exports = NStudent;