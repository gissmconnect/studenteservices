require('dotenv').config();
//const redis = require('../config/redisHelper');
const Queue = require('bull');
const uaeDocumentParsingQueue = new Queue('uae-document-parsing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST }/*redis*/,
});

module.exports = uaeDocumentParsingQueue;
