require('dotenv').config();


const documentParsingQueue = require('./queues/document-parsing-queue');
const pdfToTextQueue = require('./queues/pdfToText');
const ibDocumentParsingQueue = require('./queues/ib-document-parsing');
const uaeDocumentParsingQueue = require('./queues/uae-document-parsing-queue');
const shiDocumentParsingQueue = require('./queues/shi-document-parsing-queue');
const abaDocumentParsingQueue = require('./queues/IB-birtish-parsing-queue');
// const saveMessageQueue = require('./queues/saveMessage');

documentParsingQueue.process(require('./processes/parsing-document'));
pdfToTextQueue.process(require('./processes/pdfTotextParsing'));
ibDocumentParsingQueue.process(require('./processes/ib-document-parsing'));
uaeDocumentParsingQueue.process(require('./processes/uae-document-parsing'));
shiDocumentParsingQueue.process(require('./processes/shi-document-parsing'));
abaDocumentParsingQueue.process(require('./processes/ib-birtish-document-parsing'));
// saveMessageQueue.process(require('./processes/saveMessage'));

console.log("********** queue is running *************");