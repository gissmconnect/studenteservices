require('dotenv').config();
//const redis = require('../config/redisHelper');
const Queue = require('bull');
const storePdfQueue = new Queue('storePdf', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST }/*redis*/,
});

module.exports = storePdfQueue;
