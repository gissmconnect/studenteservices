require('dotenv').config();
//const redis = require('../config/redisHelper');
const Queue = require('bull');
const documentParsingQueue = new Queue('document-parsing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST }/*redis*/,
});

module.exports = documentParsingQueue;
