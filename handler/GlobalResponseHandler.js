"use strict";

class ResponseHandler {
    static async getResponse(ctx, statusCode, message, data,lastLoginDetail,custom) {
            ctx.meta.status = true;
            ctx.meta.statusCode = statusCode;
            ctx.meta.message = message;
            ctx.meta.payload = data;
            ctx.meta.lastLoginDetail = lastLoginDetail
            ctx.meta.student = custom
            return ctx.meta;
            
    }
}
module.exports = ResponseHandler;
