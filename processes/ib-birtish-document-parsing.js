require('dotenv').config();
const Cloud = require('@google-cloud/storage');
const fs = require('fs');
const vision = require('@google-cloud/vision');
const temp_student_marks = require('../models/temporarymarks');
const helpers = require('../parsingModule/helper');
const { Storage } = Cloud
const storage = new Storage({
    keyFilename: process.env.CLOUD_JSON,
    projectId: process.env.PROJECT_NAME,
  })
  
  const client = new vision.ImageAnnotatorClient(
    {
      keyFilename: process.env.CLOUD_JSON
    }
  );
//CONFIGURE BUCKET
const bucket = storage.bucket(process.env.BUCKET_NAME);

module.exports = async(job,done)=>{
    //make file name with 5 charecter
    const fileName = makefileName(5);
    const options = {destination :`uploads/${fileName}.json`}
    let filePath = job.data.filePath 
    console.log("******filepath*******",filePath)
    bucket.upload(filePath).then(async (result)=>{
        var filenamegcp = filePath.substring(filePath.lastIndexOf('/')+1);
        const gcsSourceUri = `gs://heacbucket/${filenamegcp}`
        const gcsDestinationUri = `gs://heacbucket/marksheet-folder/${fileName}`;

    const inputConfig = {
        // Supported mime_types are: 'application/pdf' and 'image/tiff'
        mimeType: 'application/pdf',
        gcsSource: {
        uri: gcsSourceUri,
        },
    };
    const outputConfig = {
        gcsDestination: {
        uri: gcsDestinationUri
        },
    };
    const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
    const request = {
        requests: [
        {
            inputConfig: inputConfig,
            features: features,
            outputConfig: outputConfig,
        },
        ],
    };
        const [operation] = await client.asyncBatchAnnotateFiles(request);
        const [filesResponse] = await operation.promise();
        const destinationUri =
        filesResponse.responses[0].outputConfig.gcsDestination.uri;
        await bucket.file(`marksheet-folder/${fileName}output-1-to-1.json`).download(options)
        
        let data = fs.readFileSync(`uploads/${fileName}.json`);
        let parseData = JSON.parse(data);
        // console.log(parseData.responses[0].fullTextAnnotation.text)
        // fs.writeFileSync('uploads/uae.txt',parseData.responses[0].fullTextAnnotation.text);
        let subgrade = await parseStudentData(parseData.responses[0].fullTextAnnotation.text)
        updateDB(subgrade,job.data.student_id).then(re=>done()).catch(error=>{
            console.log(error);
            done();
        })
        done();
    })

}

function makefileName(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

async function parseStudentData(data){
    let unique_subject = helpers.subjectUAE(data);
    const grades = ['A', 'B', 'C', 'D',' A',' B',' C',' D','A+', 'B+', 'C+', 'D+',' A+',' B+',' C+',' D+'];
    let arrayOfData = data.split("\n");
    //find grade index
    let subjects = [];
    let grads = [];
    let sub_grade = {};
    for(let subject of unique_subject){
        console.log(subject,"subject")
        let index = arrayOfData.indexOf(subject);
        if(grades.includes(arrayOfData[index+1]) ||  !isNaN(arrayOfData[index+1])){
            subjects.push(subject);
            grads.push(arrayOfData[index+1]);
        }
        else if(grades.includes(arrayOfData[index+3]) ||  !isNaN(arrayOfData[index+3])){
            subjects.push(subject);
            grads.push(arrayOfData[index+3]);
        }
    }

    for(let i=0;i<subjects.length;i++){
        if(grads[i].match(/^-?\d+$/) || grades.includes(grads[i])){
            sub_grade[subjects[i]] = grads[i]
        }
    }
    console.log(sub_grade,"sub grade")
    
    return sub_grade;
}

async function updateDB(subjectgrade,student_id){
    console.log(subjectgrade,student_id,"*************")
    for(let grade in subjectgrade){
        // let subjectName = subjectgrade[grade].split("-")[1]
        let payload = {};
        payload.subject_name = grade
        payload.student_id = student_id
        payload.grade_code = subjectgrade[grade]
        console.log(payload)
        let inserted_grade = await temp_student_marks.create(payload);
    }
}