require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const CAS_T_MOE_STUDENT_MARK = sequelize.define('CAS_T_MOE_STUDENT_MARK', {
    // Model attributes are defined here
    STUDENT_ID: {
      type: DataTypes.STRING(20)
    },
    SUBJECT_ID: {
        type: DataTypes.STRING(20),
        refrences:'CAS_M_SUBJECT',
        referencesKey:'SUBJECT_ID'
      },
      GRADE_CODE: {
        type: DataTypes.STRING(5)
      },
      SUBJECT_MARKS: {
        type: DataTypes.DECIMAL(8,2)
      },
      AUDIT_TRAIL_ID: {
        type: DataTypes.DECIMAL(38,2)
      },
      MARKS_PERCENTAGE: {
        type: DataTypes.DECIMAL(8,4)
      },
      REMARKS: {
        type: DataTypes.STRING(100)
      },

      STATUS: {
        type: DataTypes.STRING(5)
      },
      TOTAL_MARKS: {
        type: DataTypes.DECIMAL(8,4)
      },
      CERTIFICATE_GRADE_ID: {
        type: DataTypes.STRING(20)
      },
      SUBLEVEL_ID: {
        type: DataTypes.STRING(20)
      },
},
{
    timestamps:false,
    freezeTableName:true,
    indexes:[
        {
            name: 'unique_index',
            unique: true,
            fields: ['STUDENT_ID', 'SUBJECT_ID']
        }
    ]
}
)
CAS_T_MOE_STUDENT_MARK.sync();
module.exports = CAS_T_MOE_STUDENT_MARK;