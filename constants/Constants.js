const JWTOptions = {
    issuer:  "Mysoft corp",//TODO
    subject:  "some@user.com",//TODO
    audience:  "http://mysoftcorp.in",//TODO
    expiresIn:  "30d",    // 30 days validity
    algorithm:  "RS256"    
};

module.exports = {
    JWTOptions
}