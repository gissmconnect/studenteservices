require('dotenv').config();
//const redis = require('../config/redisHelper');
const Queue = require('bull');
const shiDocumentParsingQueue = new Queue('shi-document-parsing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST }/*redis*/,
});

module.exports = shiDocumentParsingQueue;
