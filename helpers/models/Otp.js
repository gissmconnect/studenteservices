require('dotenv').config()
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const Otp = sequelize.define('otp-model', {
    civil_num:{
        type:DataTypes.STRING,
        allowNull:false
    },
    otp:{
        type:DataTypes.STRING,
        allowNull:false
    },
    mobile_number:{
        type:DataTypes.STRING
    },
    email:{
        type:DataTypes.STRING
    },

})

Otp.sync();
module.exports = Otp;