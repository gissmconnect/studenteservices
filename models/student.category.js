require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const Disability = sequelize.define('Disability', {
  // Model attributes are defined here
  studentId:{
    type:DataTypes.STRING,
    references:{
        model:"Students",
        key:'civilNum'
    }
  },
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
    },
  studentCategory:{
    type: DataTypes.ENUM,
    values:["Low Income","Social Security","Other Type"]
  },
  socialSecurityNumber:{
    type: DataTypes.STRING
  },
  disability:{
    type: DataTypes.STRING
  },
  otherDisability:{
    type: DataTypes.STRING
  }
}, {
  // Other model options go here
});
Disability.sync();
module.exports = Disability;