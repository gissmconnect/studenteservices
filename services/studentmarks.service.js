"use strict";
const SubjectMarks = require("../models/CAS_T_MOE_STUDENT_MARK");
const SubjectDetail = require('../models/CAS_M_SUBJECT');
const Student = require('../models/CAS_H_STUDENT');
const TempMarks = require('../models/temporarymarks');


module.exports = {
	name: "marks",
    mixins: [SubjectMarks],
	/**
	 * Actions
	 */
	actions: {
		get_student_marks: {
            // auth:true,
			/** @param {Context} ctx  */
			async handler(ctx) {
                console.log("^^^^^^^^^^^^^")
				// let marks_doc = await SubjectMarks.findAll({where:{STUDENT_ID:ctx.params.STUDENT_ID}});
                let marks_doc = await TempMarks.findAll({where:{student_id:ctx.params.STUDENT_ID}});
                // let subj_data = [];
                // for(let mark_doc of marks_doc){
                //     let sub_data = await SubjectDetail.findOne({where:{SUBJECT_ID:mark_doc.dataValues.SUBJECT_ID}});
                //     mark_doc.dataValues.SUBJECT_NAME = sub_data.dataValues.SUBJECT_NAME;
                //     subj_data.push(mark_doc.dataValues);
                // }
            
                return marks_doc;
			}
		},

        add_student_marks: {
            // auth:true,
			/** @param {Context} ctx  */
			async handler(ctx) {
				let payload = ctx.params.data;
                console.log(payload)
                // let marks_doc = await SubjectMarks.bulkCreate(payload);
                let marks_doc = await TempMarks.bulkCreate(payload);
                return marks_doc;
			}
		},
        update_student_marks:{
            // auth:true,
			/** @param {Context} ctx  */
			async handler(ctx) {
                // let user_data = await Student.findOne({where:{CIVIL_NUM:ctx.meta.user}})
                let payload = ctx.params;
                let newPayload = {...payload};
                delete newPayload.subject_name;
                // for(let i=0; i<payload.length;i++){
                    // const newPayload = {}
                    // if(payload[i].subject_marks){
                    //     newPayload["subject_marks"] = payload[i].subject_marks
                    // }
                    // if(payload[i].grade_code){
                    //     newPayload["grade_code"] = payload[i].grade_code
                    // }
                    // let marks_doc = await SubjectMarks.update(newPayload,{where:{STUDENT_ID:ctx.params.STUDENT_ID,SUBJECT_ID:payload[i].SUBJECT_ID}});
                    let marks_doc = await TempMarks.update(newPayload,{where:{student_id:ctx.params.student_id,subject_name:payload.subject_name}});
                // }
                return {success:true,message:"update successfully"};
			}
        }
	}
};
