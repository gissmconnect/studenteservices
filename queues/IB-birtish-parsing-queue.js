require('dotenv').config();
//const redis = require('../config/redisHelper');
const Queue = require('bull');
const ibBirtishParsingQueue = new Queue('ib-birtish-document-parsing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST }/*redis*/,
});

module.exports = ibBirtishParsingQueue;
