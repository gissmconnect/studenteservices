require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');
const sequelize = new Sequelize(process.env.DB_URI);
const MOE_BCK_Student = sequelize.define('CAS_M_MOE_STUDENT_BCK', {
  // Model attributes are defined here
  CIVIL_NUM: {
    type: DataTypes.STRING(20)
  },
  STUDENT_ID:{
    type:DataTypes.STRING(20),
    primaryKey:true
  },
  FIRST_NAME: {
    type: DataTypes.STRING(50)
  },
  FIRST_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  SECOND_NAME: {
    type: DataTypes.STRING(50)
  },
  SECOND_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  THIRD_NAME: {
    type: DataTypes.STRING(50)
  },
  THIRD_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  FOURTH_NAME: {
    type: DataTypes.STRING(50)
  },
  FOURTH_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  DATE_OF_BIRTH:{
    type:DataTypes.DATE,
    // get() {
    //   return moment(this.getDataValue('dob')).format('DD/MM/YYYY');
    // }
  },
  PLACE_OF_BIRTH:{
    type:DataTypes.STRING(50)
},
  PLACE_OF_BIRTH_AR:{
      type:DataTypes.STRING(100)
  },
  STATUS:{
    type:DataTypes.NUMBER(2,0)
 },
 AUDIT_TRAIL_ID:{
    type:DataTypes.NUMBER(38,0)
 },
 GENDER:{
    type:DataTypes.CHAR(1)
  },
  SCHOOL_ID:{
    type:DataTypes.STRING(10)
},
SCHOOL_SEAT_NUMBER:{
    type:DataTypes.STRING(10)
},
SCHOOL_STUDY_YEAR:{
    type:DataTypes.STRING(10)
},
PHONE:{
    type:DataTypes.STRING(20)
 },
 EMAIL:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS2:{
    type:DataTypes.STRING(100)
 },
 ADDRESS2_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS3:{
    type:DataTypes.STRING(100)
 },
 ADDRESS3_AR:{
    type:DataTypes.STRING(200)
 },
 GUARDIAN_NAME:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_NAME_AR:{
    type:DataTypes.STRING(200)
 },
 RELATION_ID:{
    type:DataTypes.STRING(5)
 },
 PLACE_OF_WORK:{
    type:DataTypes.STRING(50)
 },
 PLACE_OF_WORK_AR:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_ADDRESS1:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS1_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS2:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS2_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS3:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS3_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_PO_BOX_NUM:{
    type:DataTypes.STRING(10)
  },
  GUARDIAN_POSTAL_CODE:{
    type:DataTypes.STRING(10)
  },
  GUARDIAN_WILAYAT_ID:{
    type:DataTypes.STRING(20)
  },
  WILAYAT_ID:{
    type:DataTypes.STRING(20)
  },
  GUARDIAN_GSM:{
    type:DataTypes.STRING(20)
  },
  PASSPORT_NUM:{
    type:DataTypes.STRING(20)
  },
  RELIGION_ID:{
    type:DataTypes.STRING(5)
  },

  COUNTRY_ID:{
    type:DataTypes.STRING(5),
    defaultValue:512
  },

  VILLAGE_ID:{
    type:DataTypes.STRING(20)
  },
  CERTIFICATE_ID:{
    type:DataTypes.STRING(20)
  },
  REG_STATUS:{
    type:DataTypes.NUMBER(2,0)
  },
  UP_STATUS:{
    type:DataTypes.STRING(2)
  },
  MARK_STATUS:{
    type:DataTypes.STRING(2)
  },
  NATIONALITY_PARTIAL:{
    type:DataTypes.NUMBER(2,0),
    defaultValue:0
  },
  EXAM_COUNTRY_ID:{
    type:DataTypes.STRING(5)
 },

 IS_MOE:{
    type:DataTypes.NUMBER(2,0),
    defaultValue:0
 },
 QUALIFICATION_ID:{
    type:DataTypes.STRING(10)
 },
 REGISTER_DATE:{
    type:DataTypes.DATE,
    defaultValue:Sequelize.NOW
 },
 REMARKS:{
    type:DataTypes.STRING(500)
 },
 NATIONALITY_STATUS:{
    type:DataTypes.STRING(200)
 },
 USERCHANG:{
    type:DataTypes.STRING(200)
 },
 NATIONALITY_CONVERT_DATE:{
    type:DataTypes.DATE
 },
 ELIGIBLE_STATUS:{
    type:DataTypes.STRING(1),
    defaultValue:0
 },
 TUWTINEMTIAZ:{
     type:DataTypes.STRING(3),
     defaultValue:0
 },
 IS_GCC:{
     type:DataTypes.NUMBER(0,1),
     defaultValue:0
 },   
},
{
    timestamps: false,
    freezeTableName:true
}
);

module.exports = MOE_BCK_Student;