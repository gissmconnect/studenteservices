'use strict'
const OtpModel = require('../helpers/models/Otp');
const SmsHelper = require('../helpers/smsHelper');
const otpGenerator = require('otp-generator');

module.exports = {
    name:"verification",
    mixins:[OtpModel],
    actions:{
        sendOtp:{
            /** @param {Context} ctx  */
            async handler(ctx){
                let civilNum = ctx.params.CIVIL_NUM;
                let mobileNum = ctx.params.PHONE;
                const otp = otpGenerator.generate(6, { alphabets: false, specialChars: false,upperCase:false });
                await new OtpModel({
                    civil_num:civilNum,
                    otp:/*otp*/'123456',
                    mobile_number:mobileNum
                }).save();
                const message = `Your OTP for Registeration is ${otp}.`
                console.log(message,"KKKKKKKKKKKKKKKKKKKKK")
                SmsHelper.sendSms(mobileNum,message);
                return {success:true,message:"otp successfully send"}
            }
        },
        verifyOtp:{
            /** @param {Context} ctx */
            async handler(ctx){
                let civilNum = ctx.params.CIVIL_NUM;
                let mobileNum = ctx.params.PHONE;
                let otp = ctx.params.OTP
                const otpData = await OtpModel.findOne({where:{
                    mobile_number:mobileNum,
                    civil_num:civilNum,
                    otp:otp
                }})
                if(!otpData){
                    return {
                        success:false,
                        statusCode:401,
                        message:"unauthorized"
                    }
                }
                else{
                    return {
                        success:true,
                        message:"successfull"
                    }
                }
            }
        }
    }
}