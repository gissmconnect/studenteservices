require('dotenv').config();
const {exec} = require('child_process');
const pdfToText = require('pdf-to-text'); 
const vision = require('@google-cloud/vision');
const documentParsingQueue = require('../queues/document-parsing-queue');
const pdfToTextQueue = require('../queues/pdfToText');
const ibDocumentParsingQueue = require('../queues/ib-document-parsing');
const uaeDocumentParsingQueue = require('../queues/uae-document-parsing-queue');
const shiDocumentParsingQueue = require('../queues/shi-document-parsing-queue');
const abaDocumentParsingQueue = require('../queues/IB-birtish-parsing-queue');
const CAS_H_Student = require("../models/CAS_H_STUDENT");
const imageScanner = require('image-qr-code-scanner')
const temporarymarks1 = require('../models/temporarymarks');
const fs = require('fs');
const client = new vision.ImageAnnotatorClient(
    {
      keyFilename: process.env.CLOUD_JSON
    }
  );

const parse = require('mrz').parse;

module.exports = {
	name: "ocr",
	// version: 1

	/**
	 * Mixins
	 */

	/**
	 * Actions
	 */
	actions: {
		read_ocr: {
			/** @param {Context} ctx  */
			async handler(ctx) {
                	console.log("*************hello world**************")
                	let sample2 = ctx.meta.file;
                	console.log(sample2.path,"*************file Path**************");
			/*	const [result] = await client.textDetection(sample2.path);
                const texts = result.textAnnotations;
                let mainResult = texts[0].description;
                let mrzStartPosition = mainResult.indexOf("P<");
                let mrzText = mainResult.slice(mrzStartPosition);
                let textss = mrzText.replace( /\s/g, "");
                let line1  = textss.substring(0, 44);
                let line2  = textss.substring(44, 88);
                let mrzToDecode = `${line1}\n${line2}`;
                let passportDetails = parse(mrzToDecode);
				return passportDetails.fields */
		return new Promise((resolve, reject) => {
    			exec(`mrz --json ${sample2.path}`, (err, stdout, stderr) => {
      			if(err) {
        		reject(err);
        		return {success:false};
      		}
            let jsonout = JSON.parse(stdout);
            let myObj = {};
            for(let data in jsonout){
                if(String(data)=='date_of_birth' || String(data) =='expiration_date'){
                    let tmp1 = "";
                    let tmp2 = String(jsonout[data])
                    for(let i=0;i<tmp2.length;i++){
                        if(i%2==0 && i>0){
                            tmp1 += "-"+tmp2[i]
                        }
                        else{
                            tmp1 += tmp2[i];
                        }
                    }
                    tmp1 = tmp1.split("-");
                    if(tmp1[0]<30){
                        tmp1 = "20"+String(tmp1[0])+"-"+tmp1[1]+"-"+tmp1[2];
                    }
                    else{
                        tmp1 = String(tmp1[2])+"-"+tmp1[1]+"-"+tmp1[0]
                    }
                    console.log(tmp1)
                    myObj[data] = new Date(tmp1);
                }
                else{
                    let temp = String(jsonout[data]).replace(/</g,'')
                    myObj[data] = temp;
                }
            }
			resolve({success:true,data:myObj})
			// return {success:true,data:JSON.parse(stdout)}
    		})
  	})
		/*let result = await exec(`mrz --json ${sample2.path}`,*/ /*(error, stdout, stderr) => {
			console.log(err,"err",stdout,"stdout",stderr,"stderr")
    		if (error) {
    			return {success: false,error:error.message};
        }
    	if (stderr) {
    		return {success: false,error:stderr};
	}
	else{
    	return {success:true,data:stdout}
	}}*/
	//);
	//			return {success:true, data:result}
			}
		},

        sample_ocr: {
			/** @param {Context} ctx  */
			async handler(ctx) {
                let sample2 = ctx.meta.file;
                console.log(sample2.path,"*************file Path**************");
			    const [result] = await client.textDetection(sample2.path);
                const texts = result.textAnnotations;
                let mainResult = texts[0].description;
                fs.writeFileSync('uploads/uae.txt',mainResult)
                return {success:true,texts:mainResult}
            }
        },

        read_marksheet:{
            // auth:"required",
            /** @param {Context} ctx  */
			async handler(ctx) {
                console.log("hello world",ctx.params.query.certificate)
                let sample2 = ctx.meta.file;
                let student_id = ctx.params.query.STUDENT_ID;
                const tempmarks = await temporarymarks1.findAll({where:{student_id:student_id}});
                if(tempmarks.length>1){
                    return {success:false,message:"marksheet already uploaded",statusCode:400}
                }
                //file extension(pdf/image)
                let fileExtension = /[^.]+$/.exec(sample2.path);
                if(fileExtension=='pdf'){
                    //sultan qaboos international
                    if(ctx.params.query.certificate=='SQIB'){
                        await ibDocumentParsingQueue.add({
                            filePath:sample2.path,
                            certificate:ctx.params.query.certificate,
                            student_id:student_id
                            },{
                                attempts: 2,
                                removeOnFail:true,
                                removeOnComplete:true
                            })
                    }
                    //for UAE
                    else if(ctx.params.query.certificate=='UAE'){
                        await uaeDocumentParsingQueue.add({
                            filePath:sample2.path,
                            certificate:ctx.params.query.certificate,
                            student_id:student_id
                            },{
                                attempts: 2,
                                removeOnFail:true,
                                removeOnComplete:true
                            })
                    }
                    //shofial International school
                    else if(ctx.params.query.certificate=='SHI'){
                        await shiDocumentParsingQueue.add({
                            filePath:sample2.path,
                            certificate:ctx.params.query.certificate,
                            student_id:student_id
                            },{
                                attempts: 2,
                                removeOnFail:true,
                                removeOnComplete:true
                            })
                    }
                    //american birtish
                    else if(ctx.params.query.certificate=='ABA'){
                        console.log("with in aba",ctx.params.query.certificate)
                        await abaDocumentParsingQueue.add({
                            filePath:sample2.path,
                            certificate:ctx.params.query.certificate,
                            student_id:student_id
                            },{
                                attempts: 2,
                                removeOnFail:true,
                                removeOnComplete:true
                            })
                    }
                    else{
                        await pdfToTextQueue.add({
                            filePath:sample2.path,
                            certificate:ctx.params.query.certificate,
                            student_id:student_id
                            },{
                                attempts: 2,
                                removeOnFail:true,
                                removeOnComplete:true
                            })
                        }
                }
                else{
                    let results = await imageScanner(sample2.path);
                    console.log(results.length,"KKKKKKKKKHHHHHHHHHHHHKK",String([]),String(results))
                    if(results.length<4){
                        return {status:false,message:"invalid qrcode"}
                    }
                    await documentParsingQueue.add({
                    filePath:sample2.path,
                    certificate:ctx.params.query.certificate,
                    student_id:student_id
                },{
                    attempts: 2,
                    removeOnFail:true,
                    removeOnComplete:true
                }) 
            }
                
                
                // console.log(mainResult,"main text")
                // let subject = this.subjectUAE(mainResult);
                // console.log("subject",subject)
                // this.marks(mainResult);
                // let certificate_name = this.certificateNameUAE(mainResult)
				return {success:true,message:"file uploaded successfully other process is running in background"}

        }}}
        }
