require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const CAS_M_SUBJECT = sequelize.define('CAS_M_SUBJECT', {
    // Model attributes are defined here
    SUBJECT_ID: {
        type: DataTypes.STRING(20),
        primaryKey:true,
        allowNull:false
        
      },
      SUBJECT_CODE: {
        type: DataTypes.STRING(10),
        unique:true
      },
      SUBJECT_NAME: {
        type: DataTypes.STRING(50)
      },
      SUBJECT_NAME_AR: {
        type: DataTypes.STRING(100)
      },
      TOTAL_MARKS: {
        type: DataTypes.DECIMAL(8,2)
      },
      AUDIT_TRAIL_ID: {
        type: DataTypes.DECIMAL(38,2)
      }
},
{
    timestamps:false,
    freezeTableName:true
}
)
CAS_M_SUBJECT.sync();

module.exports = CAS_M_SUBJECT;