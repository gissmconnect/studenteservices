require('dotenv').config()
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const SentMessage = sequelize.define('sent_message', {
    message_id:{
        type:DataTypes.INTEGER
    },
    lang:{
        type:DataTypes.STRING(5),
    },
    message1:{
        type:DataTypes.STRING
    },
    message2:{
        type:DataTypes.STRING
    },
    message3:{
        type:DataTypes.STRING
    },
    service_id:{
        type:DataTypes.STRING
    },
})

SentMessage.sync();

module.exports = SentMessage;
