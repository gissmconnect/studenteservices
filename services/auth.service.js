"use strict";

const Student = require("../models/CAS_H_STUDENT");
const JWT = require("../handler/jwt");
const GlobalResponseHandler = require("../handler/GlobalResponseHandler");
const bcrypt = require('bcrypt');
const loginDetail = require('../models/cas_t_student_login');
module.exports = {
	name: "auth",

	/**
	 * Actions
	 */
	actions: {
		student_login: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				const { username, password } = ctx.params;
				console.log()
				console.log({username}," == ",{password});
				let student = await Student.findOne({where:{ CIVIL_NUM: username }});
				if(!student)return GlobalResponseHandler.getResponse(ctx, 404, "User not found", {});
					
				if(!bcrypt.compareSync(password,student.PASSWORD))return GlobalResponseHandler.getResponse(ctx, 400, "Invalid credentials!", {});
				student = student.dataValues || student;
				let payload = {
					CIVIL_NUM: student.CIVIL_NUM,
					type: "SIGNIN"
				};
				
				const token = JWT.sign(payload);
				const lastLoginDetail = await loginDetail.findAll({where:{student_id:student.STUDENT_ID},order: [['updatedAt', 'DESC']]});
				console.log(lastLoginDetail,"last login detail")
				let last;
				if(lastLoginDetail.length>0){
					last = lastLoginDetail[0].dataValues;
				}
				else{
					last = {}
				}
				//custom student data
				let custom_student = {
					student_id:student.STUDENT_ID,
					email:student.EMAIL
				}
				return GlobalResponseHandler.getResponse(ctx, 200, "Login successful", { token },last,{...custom_student})
			}
		},
	}
};
