require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const cas_t_student_login = sequelize.define('cas_t_student_login', {
    login_id:{
        type:DataTypes.STRING(30),
        allowNull:false,
        primaryKey:true
    },
    student_id:{
        type:DataTypes.STRING(20),

    },
    session_id:{
        type:DataTypes.STRING(20),

    },
    login_time:{
        type:DataTypes.DATE
    },
    logout_time:{
        type:DataTypes.DATE
    },
    logout_mode:{
        type:DataTypes.INTEGER
    },
    remote_address:{
        type:DataTypes.STRING(50)
    },
    remote_port:{
        type:DataTypes.STRING(50)
    },
    remote_user:{
        type:DataTypes.STRING(150)
    }

})
cas_t_student_login.sync();
module.exports = cas_t_student_login;