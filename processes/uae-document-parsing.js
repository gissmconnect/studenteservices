require('dotenv').config();
const Cloud = require('@google-cloud/storage');
const fs = require('fs');
const vision = require('@google-cloud/vision');
const temp_student_marks = require('../models/temporarymarks');
const helper = require('../parsingModule/helper');
const { Storage } = Cloud
const storage = new Storage({
    keyFilename: process.env.CLOUD_JSON,
    projectId: process.env.PROJECT_NAME,
  })
  
  const client = new vision.ImageAnnotatorClient(
    {
      keyFilename: process.env.CLOUD_JSON
    }
  );
//CONFIGURE BUCKET
const bucket = storage.bucket(process.env.BUCKET_NAME);

module.exports = async(job,done)=>{
    try{
        //make file name with 5 charecter
        const fileName = makefileName(5);
        const options = {destination :`uploads/${fileName}.json`}
        let filePath = job.data.filePath 
        bucket.upload(filePath).then(async (result)=>{
            var filenamegcp = filePath.substring(filePath.lastIndexOf('/')+1);
            const gcsSourceUri = `gs://heacbucket/${filenamegcp}`
            const gcsDestinationUri = `gs://heacbucket/marksheet-folder/${fileName}`;

        const inputConfig = {
            // Supported mime_types are: 'application/pdf' and 'image/tiff'
            mimeType: 'application/pdf',
            gcsSource: {
            uri: gcsSourceUri,
            },
        };
        const outputConfig = {
            gcsDestination: {
            uri: gcsDestinationUri
            },
        };
        const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
        const request = {
            requests: [
            {
                inputConfig: inputConfig,
                features: features,
                outputConfig: outputConfig,
            },
            ],
        };
            const [operation] = await client.asyncBatchAnnotateFiles(request);
            const [filesResponse] = await operation.promise();
            const destinationUri =
            filesResponse.responses[0].outputConfig.gcsDestination.uri;
            await bucket.file(`marksheet-folder/${fileName}output-1-to-1.json`).download(options)
            
            let data = fs.readFileSync(`uploads/${fileName}.json`);
            let parseData = JSON.parse(data);
            console.log(parseData.responses[0].fullTextAnnotation.text)
            // fs.writeFileSync('uploads/uae.txt',parseData.responses[0].fullTextAnnotation.text);
            let subgrade = await marksUAEImage(parseData.responses[0].fullTextAnnotation.text)
            marksUaestoreInDB(subgrade,job.data.student_id).then(res2=>done()).catch(err=>done());
            // updateDB(subgrade,job.data.student_id).then(re=>done()).catch(error=>{
            //     console.log(error);
            //     done();
            // })
        })
    }catch(error){
        done()
    }

}

function makefileName(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

function marksUAEImage(lines){
    const unique_subject = helper.subjectUAE(lines);
    console.log(unique_subject,"****unique subject****")
    lines = lines.split("\n")
    console.log(unique_subject)
    const grades = ['A', 'B', 'C', 'D',' A',' B',' C',' D','A+', 'B+', 'C+', 'D+',' A+',' B+',' C+',' D+'];
    let sub_mark = {};
    for(let i=0;i<lines.length;i++){
        if(unique_subject.includes(lines[i])){
            sub_mark[lines[i]] = {};
            if(grades.includes(lines[i+1])){
                sub_mark[lines[i]].GRADE = lines[i+1];
                if(lines[i+2].match(/^-?\d+$/) || lines[i+2].match(/^\d+\.\d+$/)){
                    sub_mark[lines[i]].MARKS = lines[i+2];
                }
                
            }
            else{

                if(lines[i+1].match(/^-?\d+$/) || lines[i+1].match(/^\d+\.\d+$/)){
                    sub_mark[lines[i]].MARKS = lines[i+1];
                }
                
            }
        }
    }
    return sub_mark;
}

async function marksUaestoreInDB(marksObject,student_id){
    console.log("*******in database*********")
    for(let sub in marksObject){
        // let findSubject = await CAS_M_SUBJECT.findOne({where:{SUBJECT_NAME:
        // {[Sequelize.Op.iLike]:sub}}});
        // if(findSubject){
            let payload = {};
            // payload.STUDENT_ID = student_id;
            payload.student_id = student_id;
            payload.subject_name = sub;
            // payload.SUBJECT_ID = findSubject.dataValues.SUBJECT_ID;
            if(marksObject[sub].GRADE){
                // payload.GRADE_CODE = marksObject[sub].GRADE
                payload.grade_code = marksObject[sub].GRADE
            }
            if(marksObject[sub].MARKS){
                // payload.SUBJECT_MARKS = marksObject[sub].MARKS
                payload.subject_marks = marksObject[sub].MARKS
            }
            console.log(payload,"payload")
            // let createMarksRecord = await CAS_T_MOE_STUDENT_MARK.create(payload)
            let createMarksRecord = await temp_student_marks.create(payload)
        // }
    }
}
