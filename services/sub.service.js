"use strict";
const Subject = require("../models/CAS_M_SUBJECT");

console.log(Subject)
module.exports = {
	name: "subject",
    mixins: [Subject],
	/**
	 * Actions
	 */
	actions: {
		add_subject: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				const payload = ctx.params;
				let createdSubject = await Subject.create(payload);
                return createdSubject;
			}
		},
        get_subject:{
            /** @param {Context} ctx  */
			async handler(ctx) {
				let subject_data = await Subject.findAll();
                return subject_data;
			}
        }
	}
};
