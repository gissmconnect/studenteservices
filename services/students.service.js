"use strict";

const CAS_H_Student = require("../models/CAS_H_STUDENT");
const jwt = require('../handler/jwt');
const Student = require("../models/CAS_H_STUDENT");
const SmsHelper = require('../helpers/smsHelper');
const loginDetail = require('../models/cas_t_student_login');
// const Address = require('../models/student.address');
// const Category = require('../models/student.category');

/**
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 */

module.exports = {
	name: "students",
	// version: 1

	/**
	 * Mixins
	 */
	mixins: [CAS_H_Student],

	/**
	 * Actions
	 */
	actions: {
		get_students: {
			/** @param {Context} ctx  */
			async handler() {
				const doc = await CAS_H_Student.findAll();
				return doc;
			}
		},
		//registration
		insert_student: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				var availableNationalId = await CAS_H_Student.findOne({where:{CIVIL_NUM:ctx.params.CIVIL_NUM}});
				console.log("available",availableNationalId)
				if(availableNationalId){
					ctx = {statusCode:400,success:false,message:"National Id already exist"};
					return ctx; 
					
				}
				//default passwor
				ctx.params.PASSWORD = '12345678'
				var body = ctx.params;
				console.log({body},"---------  ")
				const doc = await CAS_H_Student.create(body);
				return doc;
			},
		},
		//registration
		check_student: {
			/** @param {Context} ctx  */
			async handler(ctx) {
				if(ctx.params.CIVIL_NUM){
					let availableNationalId = await CAS_H_Student.findOne({where:{CIVIL_NUM:ctx.params.CIVIL_NUM}});
					if(availableNationalId){
						return {status:false,message:"National ID Already Exist"}
					}
				}

				if(ctx.params.STUDENT_ID){
					let availableStudentId = await CAS_H_Student.findOne({where:{STUDENT_ID:ctx.params.STUDENT_ID}});
					if(availableStudentId){
						return {status:false,message:"Student ID Already Exist"}
					}
				}
				let Otp = Math.floor(100000 + Math.random() * 900000);
			},
		},
		//update details
		update_student:{
			auth:"required",
			/** @param {Context} ctx  */
			async handler(ctx) {
				var updateStudent = await Student.update(ctx.params,{where:{CIVIL_NUM:ctx.meta.user}})
				if(updateStudent==1){
					return {success:true,message:"update successfully"}
				}
				return {success:false,message:"Something wrong"}
			},
		},

		get_student:{
			/** @param {Context} ctx  */
			async handler(ctx){
				var student_data = await Student.findOne({where:{CIVIL_NUM:ctx.meta.user},exclude:["PASSWORD"]});
				return {success:true,data:student_data}
			}

		},

		resolveToken:{
			cache: {
				keys: ["token"],
				ttl: 60 * 60 // 1 hour
			},
			params: {
				token: "string"
			},
			async handler(ctx){
				
				if(jwt.verify(ctx.params.token)){
					return jwt.decode(ctx.params.token)
				}

			}

		},


		insert_address:{
			/** @param {Context} ctx  */
			async handler(ctx){
				var body = ctx.params;
				const doc = await Address.create(body);
				return doc;
			}

		},
		insert_category:{
			/** @param {Context} ctx  */
			async handler(ctx){
				var body = ctx.params;
				const doc = await Category.create(body);
				return doc;
			}

		},

		store_login_detail:{
			/** @param {Context} ctx  */
			async handler(ctx){
				ctx.params.login_id = String(Date.now())
				var body = ctx.params;
				const doc = await loginDetail.create(body);
				return doc;
			}

		}
		// get_all_detail:{
		// 	/** @param {Context} ctx  */
		// 	async handler(ctx){
		// 		let doc = await Student.findOne({civilNum:ctx.params.civilNum});
		// 		let doc1 = await Address.findOne({studentId:ctx.params.civilNum});
		// 		const doc2 = await Category.findOne({studentId:ctx.params.civilNum});
		// 		if(doc1){
		// 			delete doc1.dataValues["studentId"];
		// 		}
		// 		if(doc2){
		// 			delete doc2.dataValues["studentId"];
		// 		}
		// 		let combine = doc.dataValues;
		// 		if(doc1 && !doc2){
		// 			combine = {
		// 				...doc.dataValues,
		// 				...doc1.dataValues
		// 			}
		// 		}
		// 		if(doc1 && doc2){
		// 			const combine = {
		// 				...doc.dataValues,
		// 				...doc1.dataValues,
		// 				...doc2.dataValues
		// 			}
		// 		}
				
		// 		return combine;
		// 	}

		// },
		// get_address:{
		// 	/** @param {Context} ctx  */
		// 	async handler(ctx){
		// 		let doc = await Address.findOne({studentId:ctx.params.civilNum});
		// 		return doc;
		// 	}

		// },
		// get_category:{
		// 	/** @param {Context} ctx  */
		// 	async handler(ctx){
		// 		let doc = await Category.findOne({studentId:ctx.params.civilNum});
		// 		return doc;
		// 	}

		// }
	}
};
