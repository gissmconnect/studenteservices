var pdfUtil = require('pdf-to-text');
const helper = require('../parsingModule/helper');
const TempMarks = require('../models/temporarymarks');

module.exports = async(job,done)=>{
    let pdf_path = job.data.filePath;
    console.log("*****pdf******")
    pdfUtil.pdfToText(pdf_path, function(err, data) {
        if (err) throw(err);
        let converted = data.split("  ");
        let converted_data = [];
        for(let line of converted){
          if(line=="") continue;
          converted_data.push(line)
        }
        console.log(converted_data,"converted data")
        let marks_uae = marksUAEImage(converted_data);
        marksUaestoreInDB(marks_uae,job.data.student_id).then(res=>{

        })        
        done();
      });
    
}

function marksUAEImage(lines){
    const unique_subject = helper.subjectUAE(lines);
    // lines = lines.split("\n")
    const grades = ['A', 'B', 'C', 'D',' A',' B',' C',' D','A+', 'B+', 'C+', 'D+',' A+',' B+',' C+',' D+'];
    let sub_marks_array = []
    for(let i=0;i<unique_subject.length;i++){
            let sub_mark = {};
            let line = unique_subject[i].split(",");
            console.log(line)
            sub_mark["subject_name"] = line[1];
            if(grades.includes(line[2])){
                sub_mark["grade"] = line[2].replace(" ","");
                if(line[3].match(/^-?\d+$/) || line[3].match(/^\d+\.\d+$/)){
                    sub_mark["subject_marks"] = line[3];
                }
                
            }
            else{

                if(line[2].match(/^-?\d+$/) || line[2].match(/^\d+\.\d+$/)){
                    sub_mark["subject_marks"] = lines[2];
                }
            }
            sub_marks_array.push(sub_mark)
    }
    return sub_marks_array;
}

async function marksUaestoreInDB(marksObject,student_id){
    console.log("*******in database*********",marksObject)
    for(let i=0; i<marksObject.length; i++){
        // let findSubject = await CAS_M_SUBJECT.findOne({where:{SUBJECT_NAME:
        // {[Sequelize.Op.iLike]:sub}}});
        // if(findSubject){
            let payload = {};
            // payload.STUDENT_ID = student_id;
            payload.student_id = student_id;
            payload.subject_name = marksObject[i].subject_name;
            // payload.SUBJECT_ID = findSubject.dataValues.SUBJECT_ID;
            if(marksObject[i].grade){
                // payload.GRADE_CODE = marksObject[sub].GRADE
                payload.grade_code = marksObject[i].grade
            }
            if(marksObject[i].subject_marks){
                // payload.SUBJECT_MARKS = marksObject[sub].MARKS
                payload.subject_marks = marksObject[i].subject_marks
            }
            console.log(payload,"payload")
            // let createMarksRecord = await CAS_T_MOE_STUDENT_MARK.create(payload)
            let createMarksRecord = await TempMarks.create(payload)
        // }
    }
}

