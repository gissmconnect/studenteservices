
const helper = require('../parsingModule/helper');
const vision = require('@google-cloud/vision');
const CAS_T_MOE_STUDENT_MARK = require('../models/CAS_T_MOE_STUDENT_MARK');
const CAS_M_SUBJECT = require('../models/CAS_M_SUBJECT');
const TempMarks = require('../models/temporarymarks');
const Sequelize = require('sequelize')
const client = new vision.ImageAnnotatorClient(
    {
      keyFilename: process.env.CLOUD_JSON
    }
  );
module.exports = async function (job, done) {
    let filePath = job.data.filePath;
    const [result] = await client.textDetection(filePath);
    const texts = result.textAnnotations;
    let mainResult = texts[0].description;
    let marks_uae = marksUAEImage(mainResult);
    marksUaestoreInDB(marks_uae,job.data.student_id).then(res=>{

    })
    // let convertedData = data.split("  ");
    // let lines_data = [];
    // for(let i of convertedData){
    //     if(i==""){
    //         continue;
    //     }
    //     lines_data.push(i)
    // }
    // // console.log(lines_data)
    // marksUAE(lines_data);
    done(null);
}


function marksUAEImage(lines){
    const unique_subject = helper.subjectUAE(lines);
    console.log(unique_subject,"****unique subject****")
    lines = lines.split("\n")
    console.log(unique_subject)
    const grades = ['A', 'B', 'C', 'D',' A',' B',' C',' D','A+', 'B+', 'C+', 'D+',' A+',' B+',' C+',' D+'];
    let sub_mark = {};
    for(let i=0;i<lines.length;i++){
        if(unique_subject.includes(lines[i])){
            sub_mark[lines[i]] = {};
            if(grades.includes(lines[i+1])){
                sub_mark[lines[i]].GRADE = lines[i+1];
                if(lines[i+2].match(/^-?\d+$/) || lines[i+2].match(/^\d+\.\d+$/)){
                    sub_mark[lines[i]].MARKS = lines[i+2];
                }
                
            }
            else{

                if(lines[i+1].match(/^-?\d+$/) || lines[i+1].match(/^\d+\.\d+$/)){
                    sub_mark[lines[i]].MARKS = lines[i+1];
                }
            }
        }
    }
    return sub_mark;
}

// for(let i of unique_subject){
//     let marks_line = i.split(",");
//     sub_mark[marks_line[1]] = {};
//     sub_mark[marks_line[1]].GRADE = marks_line[2];
//     sub_mark[marks_line[1]].MARKS = marks_line[3];  
//     sub_mark[marks_line[1]].ArabicName = marks_line[4]
// }


async function marksUaestoreInDB(marksObject,student_id){
    console.log("*******in database*********")
    for(let sub in marksObject){
        // let findSubject = await CAS_M_SUBJECT.findOne({where:{SUBJECT_NAME:
        // {[Sequelize.Op.iLike]:sub}}});
        // if(findSubject){
            let payload = {};
            // payload.STUDENT_ID = student_id;
            payload.student_id = student_id;
            payload.subject_name = sub;
            // payload.SUBJECT_ID = findSubject.dataValues.SUBJECT_ID;
            if(marksObject[sub].GRADE){
                // payload.GRADE_CODE = marksObject[sub].GRADE
                payload.grade_code = marksObject[sub].GRADE
            }
            if(marksObject[sub].MARKS){
                // payload.SUBJECT_MARKS = marksObject[sub].MARKS
                payload.subject_marks = marksObject[sub].MARKS
            }
            console.log(payload,"payload")
            // let createMarksRecord = await CAS_T_MOE_STUDENT_MARK.create(payload)
            let createMarksRecord = await TempMarks.create(payload)
        // }
    }
}