require('dotenv').config();
const Cloud = require('@google-cloud/storage');
const fs = require('fs');
const vision = require('@google-cloud/vision');
const temp_student_marks = require('../models/temporarymarks');
const helper = require('../parsingModule/helper');
const { Storage } = Cloud
const storage = new Storage({
    keyFilename: process.env.CLOUD_JSON,
    projectId: process.env.PROJECT_NAME,
  })
  
  const client = new vision.ImageAnnotatorClient(
    {
      keyFilename: process.env.CLOUD_JSON
    }
  );
//CONFIGURE BUCKET
const bucket = storage.bucket(process.env.BUCKET_NAME);

module.exports = async(job,done)=>{
    //make file name with 5 charecter
    const fileName = makefileName(5);
    const options = {destination :`uploads/${fileName}.json`}
    let filePath = job.data.filePath 
    bucket.upload(filePath).then(async (result)=>{
        var filenamegcp = filePath.substring(filePath.lastIndexOf('/')+1);
        const gcsSourceUri = `gs://heacbucket/${filenamegcp}`
        const gcsDestinationUri = `gs://heacbucket/marksheet-folder/${fileName}`;

    const inputConfig = {
        // Supported mime_types are: 'application/pdf' and 'image/tiff'
        mimeType: 'application/pdf',
        gcsSource: {
        uri: gcsSourceUri,
        },
    };
    const outputConfig = {
        gcsDestination: {
        uri: gcsDestinationUri
        },
    };
    const features = [{type: 'DOCUMENT_TEXT_DETECTION'}];
    const request = {
        requests: [
        {
            inputConfig: inputConfig,
            features: features,
            outputConfig: outputConfig,
        },
        ],
    };
        const [operation] = await client.asyncBatchAnnotateFiles(request);
        const [filesResponse] = await operation.promise();
        const destinationUri =
        filesResponse.responses[0].outputConfig.gcsDestination.uri;
        await bucket.file(`marksheet-folder/${fileName}output-1-to-1.json`).download(options)
        
        let data = fs.readFileSync(`uploads/${fileName}.json`);
        let parseData = JSON.parse(data);
        fs.writeFileSync('uploads/uae.txt',parseData.responses[0].fullTextAnnotation.text);
        let subgrade = await marksSHIImage(parseData.responses[0].fullTextAnnotation.text)
        console.log(subgrade,"sub grade");
        marksSHIstoreInDB(subgrade,job.data.student_id).then(res2=>done()).catch(err=>done());
        
    })

}

function makefileName(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * 
 charactersLength));
   }
   return result;
}

function marksSHIImage(lines){
    //get all subject
    const unique_subject = [];
    const linesArray = lines.split("\n");
    //first subject postion
    let index = linesArray.findIndex(elem=>elem=='Subjects')
    console.log(index,"index")
    let sub_marks = {}
    let subject = [];
    let marks = []
    for(i=index+7;i<index+14;i++){
        subject.push(linesArray[i]);
    }
    for(i=index+14;i<index+21;i++){
        marks.push(linesArray[i]);
    }

    for(let i=0;i<subject.length;i++){
        if(marks[i].match(/^-?\d+$/)){
            sub_marks[subject[i]] = marks[i]
        }
    }
    return sub_marks;
}

async function marksSHIstoreInDB(marksObject,student_id){
    console.log("*******in database*********")
    for(let sub in marksObject){
        // let findSubject = await CAS_M_SUBJECT.findOne({where:{SUBJECT_NAME:
        // {[Sequelize.Op.iLike]:sub}}});
        // if(findSubject){
            let payload = {};
            // payload.STUDENT_ID = student_id;
            payload.student_id = student_id;
            payload.subject_name = sub;
            payload.subject_marks = marksObject[sub];
            console.log(payload)
            let inserted_data = await temp_student_marks.create(payload);
    }
}
