module.exports.subjectUAE = (data)=>{
    let sub = {}
    let subjects = []
    // sub[1] = data.match(new  RegExp('.*islamic.*',"i"))[0]
    // sub[2] = data.match(new  RegExp('.*Arabic.*',"i"))[0]
    // sub[3] = data.match(new  RegExp('.*Social.*',"i"))[0]
    // sub[4] = data.match(new  RegExp('.*English.*',"i"))[0]
    // sub[5] = data.match(new  RegExp('.*Math.*',"i"))[0]
    // sub[6] = data.match(new  RegExp('.*phy.*',"i"))[0]
    // sub[7] = data.match(new  RegExp('.*bio.*',"i"))[0]
    // sub[8] = data.match(new  RegExp('.*health.*',"i"))[0]
    // sub[9] = data.match(new  RegExp('.*french.*',"i"))[0]
    // sub[10] = data.match(new  RegExp('.*design.*',"i"))[0]
    // sub[11] = data.match(new  RegExp('.*arts.*',"i"))[0]
    // sub[12] = data.match(new  RegExp('.*drama.*',"i"))[0]
    // sub[13] = data.match(new  RegExp('.*studies.*',"i"))[0]
    // let chem = data.match(new  RegExp('.*chem.*',"i"))
    // sub[1] = new RegExp('.*islamic.*')
    // for(let i in sub){
    //     console.log(sub[i],"sub[i]")
    //     let matched = sub[i].compile(data)
    //     array_of_subject.push(matched)
    // }

    sub[1] = new RegExp('.*islamic.*',"i")
    sub[2] = new RegExp('.*arabic.*',"i")
    sub[3] = new RegExp('.*Social.*',"i")
    sub[4] = new RegExp('.*English.*',"i")
    sub[5] = new RegExp('.*Math.*',"i")
    sub[6] = new RegExp('.*Physics.*',"i")
    sub[7] = new RegExp('.*Biology.*',"i")
    sub[8] = new RegExp('.*Health.*',"i")
    sub[9] = new RegExp('.*French.*',"i")
    sub[10] = new RegExp('.*Design.*',"i")
    sub[11] = new RegExp('.*Arts.*',"i")
    sub[12] = new RegExp('.*Drama.*',"i")
    sub[13] = new RegExp('.*Computer.*',"i")
    sub[14] = new RegExp('.*chemistry.*',"i")
    sub[15] = new RegExp('.*Music.*',"i")
    sub[16] = new RegExp('.*Behavior.*',"i")
    sub[17] = new RegExp('.*Statistics.*',"i")
    sub[18] = new RegExp('.*Program.*',"i")
    sub[19] = new RegExp('.*Html.*',"i")
    sub[20] = new RegExp('.*Web.*',"i")
    sub[21] = new RegExp('.*mathematics.*',"i")
    sub[22] = new RegExp('.*Business.*',"i")
    sub[23] = new RegExp('.*Psychology.*',"i")
    sub[24] = new RegExp('.*Math.*',"i")    
    sub[25] = new RegExp('.*Spanish.*',"i")
    sub[26] = new RegExp('.*Theory Of Knowledge.*',"i")
    sub[27] = new RegExp('.*EE.*',"i")
    sub[28] = new RegExp('.*Economics.*',"i")
    sub[29] = new RegExp('.*Extended.*',"i")
    
    for (var i = 1; i < 30; i++){
        subj = sub[i].exec(data);
        if (subj){
            subjects.push(subj[0]);   
    }

    }
    let unqSubjects = Array.from(new Set(subjects));
    return unqSubjects;
}