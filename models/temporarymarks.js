require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const temp_student_marks = sequelize.define('temp_student_marks', {
    student_id:{
        type:DataTypes.STRING(20),
        allowNull:false
    },
    subject_name:{
        type:DataTypes.STRING(50),
        allowNull:false
    },
    subject_marks:{
        type:DataTypes.DECIMAL(8,2),
        // allowNull:false
    },
    grade_code:{
        type:DataTypes.STRING(5)
    },
},{
    freezeTableName:true,
    indexes:[
        {
            name: 'unique_index_temp_marks',
            unique: true,
            fields: ['subject_name', 'student_id']
        }
    ]
});
temp_student_marks.sync();

module.exports = temp_student_marks;