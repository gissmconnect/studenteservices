require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const sequelize = new Sequelize(process.env.DB_URI);
const Address = sequelize.define('Address', {
  // Model attributes are defined here
  studentId:{
    type:DataTypes.STRING,
    allowNull:false,
    references:{
        model:"Students",
        key:'civilNum'
    }
  },
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
    },
  schoolRegionName:{
    type: DataTypes.STRING
  },
  schoolWilayatName:{
    type: DataTypes.STRING
  },
  studentWilayat:{
    type: DataTypes.STRING
  },
  village:{
    type: DataTypes.STRING
  },
  latlong:{
      type:DataTypes.GEOMETRY('POINT')
  }

}, {
  // Other model options go here
});
Address.sync();
module.exports = Address;