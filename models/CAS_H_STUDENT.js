require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');
const sequelize = new Sequelize(process.env.DB_URI);
const Student = sequelize.define('CAS_H_STUDENT', {
  // Model attributes are defined here
  STUDENT_ID: {
    type: DataTypes.STRING(20)
  },
  CIVIL_NUM: {
    type: DataTypes.STRING(20),
    unique:true,
    allowNull:false
  },
  FIRST_NAME: {
    type: DataTypes.STRING(50)
  },
  FIRST_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  SECOND_NAME: {
    type: DataTypes.STRING(50)
  },
  SECOND_NAME_AR: {
    type: DataTypes.STRING(100)
  },
  THIRD_NAME: {
    type: DataTypes.STRING(50)
  },
  THIRD_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  FOURTH_NAME: {
    type: DataTypes.STRING(50)
  },
  FOURTH_NAME_AR:{
    type: DataTypes.STRING(100)
  },

  FAMILY_NAME: {
    type: DataTypes.STRING(50)
  },
  FAMILY_NAME_AR:{
    type: DataTypes.STRING(100)
  },
  GENDER:{
    type:DataTypes.CHAR(1)
  },
  DATE_OF_BIRTH:{
    type:DataTypes.DATE,
    // get() {
    //   return moment(this.getDataValue('dob')).format('DD/MM/YYYY');
    // }
  },
  PLACE_OF_BIRTH_AR:{
    type:DataTypes.STRING(100)
 },
 STUDENT_TYPE:{
    type:DataTypes.DECIMAL(2,0)
 },
 STATUS:{
    type:DataTypes.DECIMAL(2,0)
 },
 AUDIT_TRAIL_ID:{
    type:DataTypes.DECIMAL(38,0)
 },
 EXAM_COUNTRY_ID:{
    type:DataTypes.STRING(5)
 },
 SCHOOL_ID:{
    type:DataTypes.STRING(10)
 },

 SCHOOL_SEAT_NUMBER:{
    type:DataTypes.STRING(10)
 },
 SCHOOL_STUDY_YEAR:{
    type:DataTypes.STRING(10)
 },
 PHONE:{
    type:DataTypes.STRING(20)
 },
 EMAIL:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1:{
    type:DataTypes.STRING(100)
 },
 ADDRESS1_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS2:{
    type:DataTypes.STRING(100)
 },
 ADDRESS2_AR:{
    type:DataTypes.STRING(200)
 },
 ADDRESS3:{
    type:DataTypes.STRING(100)
 },
 ADDRESS3_AR:{
    type:DataTypes.STRING(200)
 },
 GUARDIAN_NAME:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_NAME_AR:{
    type:DataTypes.STRING(200)
 },
 RELATION_ID:{
    type:DataTypes.STRING(5)
 },
 PLACE_OF_WORK:{
    type:DataTypes.STRING(50)
 },
 PLACE_OF_WORK_AR:{
    type:DataTypes.STRING(100)
 },
 GUARDIAN_ADDRESS1:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS1_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS2:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS2_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_ADDRESS3:{
  type:DataTypes.STRING(100)
},
GUARDIAN_ADDRESS3_AR:{
  type:DataTypes.STRING(200)
},
GUARDIAN_PO_BOX_NUM:{
  type:DataTypes.STRING(10)
},
GUARDIAN_POSTAL_CODE:{
  type:DataTypes.STRING(10)
},
GUARDIAN_WILAYAT_ID:{
  type:DataTypes.STRING(20)
},
WILAYAT_ID:{
  type:DataTypes.STRING(20)
},
GUARDIAN_GSM:{
  type:DataTypes.STRING(20)
},
EXAM_NAME:{
  type:DataTypes.STRING(100)
},
EXAM_NAME_AR:{
  type:DataTypes.STRING(200)
},
COUNTRY_ID:{
  type:DataTypes.STRING(5)
},
EMPLOYMENT_DETAIL:{
  type:DataTypes.STRING(4000)
},
PASSPORT_NUM:{
  type:DataTypes.STRING(20)
},
CAO_NUM:{
  type:DataTypes.STRING(20)
},
RELIGION_ID:{
  type:DataTypes.STRING(5)
},
GSM:{
  type:DataTypes.STRING(20)
},
LAST_UPDATE_DATE:{
  type:DataTypes.DATE
},
EXAM_DATE:{
  type:DataTypes.DATE
},
MOE_STUDENT_ID:{
  type:DataTypes.STRING(20)
},
SOCIAL_SECURITY_NUM:{
  type:DataTypes.STRING(20)
},
REGISTER_DATE:{
  type:DataTypes.DATE
},
PASSWORD:{
  type:DataTypes.STRING(500),
  allowNull:false
},

VERIFIED_DATE:{
  type:DataTypes.DATE
},

VERIFIED_REMARK:{
  type:DataTypes.STRING(200)
},

VERIFIED_SSN:{
  type:DataTypes.DECIMAL(1,0)
},
REMARK:{
  type:DataTypes.STRING(200)
},
MARK_VERSION:{
  type:DataTypes.DECIMAL(1,0)
},
NO_MORE_OFFER:{
  type:DataTypes.DECIMAL(1,0)
},
GSM2:{
  type:DataTypes.STRING(20)
},
VILLAGE_ID:{
  type:DataTypes.STRING(20)
},
CERTIFICATE_ID:{
  type:DataTypes.STRING(20)
},
SCHOLARSHIP_TYPE:{
  type:DataTypes.DECIMAL(2,0)
},
CONVERT_DATE:{
  type:DataTypes.DATE
},
ELIGIBLE_COUNT:{
  type:DataTypes.DECIMAL(5,0)
},

SOCIAL_SECURITY_ISSUE_CENTRE:{
  type:DataTypes.STRING(20)
},
AGE:{
  type:DataTypes.INTEGER
},

GSM_COUNTRY_ID:{
  type:DataTypes.STRING(5)
},

GSM_COUNTRY_CODE:{
  type:DataTypes.STRING(5)
},

BANK_ID:{
  type:DataTypes.STRING(10)
},

BANK_ACCOUNT_NUMBER:{
  type:DataTypes.STRING(40)
},

VERIFIED_BANK:{
  type:DataTypes.DECIMAL(1,0)
},

BANK_VERIFIED_DATE:{
  type:DataTypes.DATE
},

REG_STATUS:{
  type:DataTypes.DECIMAL(2,0)
},

REGISTRATION_MODE:{
  type:DataTypes.STRING(15)
},

STUDENT_WILAYAT_ID:{
  type:DataTypes.STRING(20)
},

NATIONALITY_PARTIAL:{
  type:DataTypes.DECIMAL(2,0)
},

GCE_STUDENT_CONFIRM:{
  type:DataTypes.DECIMAL(2,0)
},
CHANGE_DATE:{
  type:DataTypes.DATE,
  defaultValue:Sequelize.NOW
},

IS_DISABLE:{
  type:DataTypes.DECIMAL(3,0)
},

DISABLE_ID:{
  type:DataTypes.STRING(20)
},

QUALIFICATION_ID:{
  type:DataTypes.STRING(10)
},

VERIFIED_DISABILITY:{
  type:DataTypes.DECIMAL(1,0)
},

DISABILITY_VERIFIED_DATE:{
  type:DataTypes.DATE
},

DISABILITY_REMARK:{
  type:DataTypes.STRING(200)
},

REGISTERED_LINK:{
  type:DataTypes.DECIMAL(1,0),
  defaultValue:1
},

IS_CONCESSION:{
  type:DataTypes.DECIMAL(1,0),
  defaultValue:0
},

VERIFIED_CONCESSION:{
  type:DataTypes.DECIMAL(1,0),
  defaultValue:0
},

VERIFIED_CONCESSION_DATE:{
  type:DataTypes.DATE
},

CONCESSION_USER_ID:{
  type:DataTypes.STRING(20),
  defaultValue:0
},

MEDICAL_STATUS:{
  type:DataTypes.DECIMAL(1,0),
  defaultValue:2
},

MEDICAL_VERIFIED_DATE:{
  type:DataTypes.DATE,
  defaultValue:0
},

MEDICAL_VERIFIED_USERID:{
  type:DataTypes.STRING(20),
},

UNCONVERT_DATE:{
  type:DataTypes.DATE,
},

UNCONVERT_USER_ID:{
  type:DataTypes.STRING(20),
},

MARK_ELIGIBILITY_OVERRIDE:{
  type:DataTypes.STRING(2),
  defaultValue:0
},

AUDIT_VERIFY_STATUS:{
  type:DataTypes.STRING(1)
},

AVG_MARK:{
  type:DataTypes.DECIMAL(8,4)
},

ID_CARD_EXPIRY_DATE:{
  type:DataTypes.DATE
},

BIRT_PLACE_COUNTRY_ID:{
  type:DataTypes.STRING(20)
},

BIRT_WILAYAT_ID:{
  type:DataTypes.STRING(20)
},

IS_MEDICAL_ISSUE:{
  type:DataTypes.DECIMAL(0,1)
},

PREV_CITIZENSHIP_COUNTRY_ID:{
  type:DataTypes.STRING(20)
},

GUARDIAN_RELATIONSHIP_ID:{
  type:DataTypes.STRING(20)
},

GUARDIAN_WORK_PHONE:{
  type:DataTypes.STRING(20)
},

PASSPORT_ISSUE_DATE:{
  type:DataTypes.DATE
},

PASSPORT_EXPIRY_DATE:{
  type:DataTypes.DATE
},
IS_DETAILS_CHANGE:{
  type:DataTypes.STRING(10),
  defaultValue:0
},
AGREE_TERMS:{
  type:DataTypes.DECIMAL(4,0),
  defaultValue:0
},
},
 {
   freezeTableName:true,
  // Other model options go here
    hooks: {
      beforeCreate: async (user) => {
       if (user.PASSWORD) {
        const salt = await bcrypt.genSaltSync(10, 'a');
        user.PASSWORD = bcrypt.hashSync(user.PASSWORD, salt);
        console.log(user.PASSWORD,"user password")
       }
      },
      beforeUpdate:async (user) => {
       if (user.PASSWORD) {
        const salt = await bcrypt.genSaltSync(10, 'a');
        user.PASSWORD = bcrypt.hashSync(user.PASSWORD, salt);
       }
      }
     },
     instanceMethods: {
      validPassword: (PASSWORD) => {
       return bcrypt.compareSync(PASSWORD, this.PASSWORD);
      }
     }
});
Student.sync();
module.exports = Student;