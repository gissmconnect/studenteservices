require('dotenv').config()
const moment = require('moment');
const { Sequelize, DataTypes } = require('sequelize');
const bcrypt = require('bcrypt');
const sequelize = new Sequelize(process.env.DB_URI);
const Recent_Student = sequelize.define('CAS_M_MOE_RECENT_STUDENT', {
  // Model attributes are defined here
  CIVIL_NUM: {
    type: DataTypes.STRING(20)
  },
  STUDENT_ID:{
    type:DataTypes.STRING(20),
    primaryKey:true
  },
},
{
    timestamps: false,
    freezeTableName:true
}
)

module.exports = Recent_Student;